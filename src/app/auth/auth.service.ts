import { Injectable } from '@angular/core';
import {  HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import {User } from './user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn = new BehaviorSubject<boolean>(false); // {1}
  private loginResponse = new BehaviorSubject<string>('');
  get isLoggedIn() {
    return this.loggedIn.asObservable(); // {2}
  }

  get loginresponseData() {
    return this.loginResponse.asObservable();
  }

  constructor( private router: Router, private http: HttpClient) { }

  login(user: User): any {
    console.log('here ' + user);
    if (user.userName !== '' && user.password !== '' ) { // {3}
      this.http.post<any>(environment.apiHost + environment.api_loginmethod, user).subscribe(
        result => {
          if (result.message === 'Auth Sucess') {
            localStorage.setItem('token', result.token);
            this.loggedIn.next(true);
            this.loginResponse.next('Sucess');
            console.log('loggin in');
            this.router.navigate(['/Dashboard']);
          } else {
            console.log(result);
            this.loginResponse.next('failed');
          }
        },
        error => {
          this.loggedIn.next(false);
          this.loginResponse.next('failed');
        }
      );
    }
  }

  logout() {                            // {4}
    this.loggedIn.next(false);
    this.router.navigate(['/login']);
  }
}
