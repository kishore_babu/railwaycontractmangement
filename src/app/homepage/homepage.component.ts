import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor() { }
  earings = false;
  expendeture = false;
  serviceContract = false;

  ngOnInit() {
    this.earings = false;
    this.expendeture = false;
    this.serviceContract = false;
  }
  DisplayComponent(type) {
    if (type === 1) {
      this.earings = true;
      this.expendeture = false;
      this.serviceContract = false;
    }
    if (type === 2) {
      this.earings = false;
      this.expendeture = true;
      this.serviceContract = false;
    }
    if (type === 3) {
      this.earings = false;
      this.expendeture = false;
      this.serviceContract = true;
    }
  }

}
