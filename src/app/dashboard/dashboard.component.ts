import { Component, OnInit } from '@angular/core';
import { ContractServicesService } from '../Services/contract-services.service';
import { formatPercent } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor( private contractService: ContractServicesService) { }
  EarningsCount = 'Loading';
  earningContractList;
  ExpenditureCount='Loading';
  expenditureContractList;
  ServiceCount='Loading';
  serviceContractList;
  ngOnInit() {
  this.getEarningContractList();
  this.getExpenditureContractList();
  this.getServiceContractList();
  }
  getEarningContractList() {
      this.contractService.getEarningsContract().subscribe(data => {
        console.log(' Earnigs :', data);
        this.earningContractList = data;
        this.EarningsCount = this.earningContractList.length;
      },
      (error: any) => {
        this.EarningsCount = '0';
      });
  }


  getExpenditureContractList() {
      this.contractService.getExpenditureContract().subscribe(data => {
        console.log(data);
        this.expenditureContractList = data;
        this.ExpenditureCount = this.expenditureContractList.length;
      },
      (error: any) => {
        this.ExpenditureCount = '0';
      });
   
  }
  getServiceContractList() {
      this.contractService.getServiceContract().subscribe(data => {
        this.serviceContractList = data;
        this.ServiceCount = this.serviceContractList.length;
      },
      (error: any) => {
        this.ServiceCount = '0';
      });
     
  } 
}
