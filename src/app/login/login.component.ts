import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';


import { AuthService } from './../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private formSubmitAttempt: boolean;
  loginForm: FormGroup;
  errorMessage = '';
  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {

  }

  ngOnInit(): void {
    this.loginForm =  this.fb.group(
      {
        userName: ['', [Validators.required, Validators.minLength(2)]],
        password: ['', [Validators.required, Validators.minLength(2)]]
      }
    );
  }
  onSubmit() {
    if (this.loginForm.valid) {
      console.log(this.loginForm.value);
      this.authService.login(this.loginForm.value);
      this.authService.loginresponseData.subscribe(
        result => {
          console.log('Result :' + result);
          if (result === 'failed'){
              this.errorMessage = 'Please check user name or password is invalid';
          }
      },
      error => {
        console.log('result :' + error);
      }) ;
    }
    this.formSubmitAttempt = true;
    console.log('Here');
  }
}
