import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import {LoginComponent } from './login/login.component';

import {AuthGuard} from '../app/auth/auth.guard';
import { EarningsContractsComponent } from './earnings-contracts/earnings-contracts.component';
import { ExpenditureContractComponent } from './expenditure-contract/expenditure-contract.component';
import { ServicesContractComponent } from './services-contract/services-contract.component';
import { ExceluploadComponent } from './excelupload/excelupload.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [

  { path: '*', component: LoginComponent},
  { path: 'login', component: LoginComponent},
  { path: 'Home', component: HomepageComponent, canActivate: [AuthGuard] },
  { path: 'Dashboard', component: DashboardComponent , canActivate: [AuthGuard]  },
  { path: 'EarningsContract', component: EarningsContractsComponent , canActivate: [AuthGuard]  },
  { path: 'ExpenditureContract', component: ExpenditureContractComponent , canActivate: [AuthGuard]  },
  { path: 'ServicesContract', component: ServicesContractComponent , canActivate: [AuthGuard]  },
  { path: 'ExcelUpload', component: ExceluploadComponent , canActivate: [AuthGuard]  },
  { path: '', redirectTo: '/login' , pathMatch: 'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
