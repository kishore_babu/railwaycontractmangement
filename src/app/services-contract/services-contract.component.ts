import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContractServicesService } from '../Services/contract-services.service';

@Component({
  selector: 'app-services-contract',
  templateUrl: './services-contract.component.html',
  styleUrls: ['./services-contract.component.css']
})
export class ServicesContractComponent implements OnInit {

  constructor(private fb: FormBuilder, private contractService: ContractServicesService) { }
  serviceContractForm: FormGroup;
  contractStartDate: any;
  contractEndDate: any;
  message: any = '';
  stationMasterData: any;
  serviceContractTypes: any;

  displayListData = false;
  serviceContractList: any;
  btn_Text = 'Add New Contract';
  ngOnInit() {

    this.getServiceContractList();
    this.getStationMaster();
    this.serviceContractTypes =
    [
      { id: 1 , Name: 'Halt Agents'},
      { id: 2 , Name: 'JTBS'},
      { id: 3 , Name: 'STBA'},
      { id: 4 , Name: 'YTSK'},
      { id: 5 , Name: 'ATVM'}
    ];

    this.serviceContractForm = this.fb.group(
      {
        NameOfLicesee : ['', [Validators.required, Validators.minLength(2)]],
        ownerName : ['', [Validators.required, Validators.minLength(2)]],
        phoneNumber : ['', [Validators.required, Validators.minLength(2)]],
        emailId : ['', [Validators.required, Validators.minLength(2), Validators.email]],
        NoofYearsContract : ['', [Validators.required, Validators.minLength(2)]],
        modeofPaymentType : [null, [Validators.required]],
        conStartdate : [null, [Validators.required]],
        conEnddate : [null, [Validators.required]],
        PercentageCommission : [0, [Validators.required, Validators.minLength(2)]],
        serviceContractSelect : [ null, [Validators.required]],
        StationList : [null, [ Validators.required]]
      });
  }
  getStationMaster() {
    this.stationMasterData =
    [ {id : 1, StationName : 'Tirupathi', StationCode : 'TPTY'},
    {id : 2, StationName : 'Guntakal Junction', StationCode : 'GTL'}];
  }
  maketwodigits(value: any) {
    let finalvalue;
    if ( value < 10) {
      finalvalue = value = '0' + value;
    } else {
      finalvalue = value;
    }
    return finalvalue;
  }

  getServiceContractList() {
    if ( this.displayListData === false){
      this.btn_Text = 'Add New Contract';
      this.contractService.getServiceContract().subscribe(data => {
        console.log(data);
        this.serviceContractList = data;
      },
      (error: any) => {
        this.message = 'Error in getting data';
      });
    } else {
      this.btn_Text = 'View Contract';
    }
    this.displayListData = !this.displayListData;
  } 
  createServiceContract(): any {
    console.log('testing ');
    console.log(this.serviceContractForm.value);
    if (this.serviceContractForm.valid) {
      console.log(this.serviceContractForm.value);
      this.serviceContractForm.patchValue(
        {
          conStartdate : ( this.contractStartDate.year + '-'
                        + this.maketwodigits(this.contractStartDate.month) + '-'
                        + this.maketwodigits(this.contractStartDate.day)),
          conEnddate : (this.contractEndDate.year + '-'
                  + this.maketwodigits(this.contractEndDate.month) + '-'
                  + this.maketwodigits(this.contractEndDate.day))
        }
        );
      this.contractService.createServiceContract(this.serviceContractForm.value).subscribe(
        result => {
          console.log(result);
          this.message = 'Service Contract created';
          this.serviceContractForm.reset();
        },
        (error: any) => {
          this.message = 'Error in creating shop';
        });
    }
  }

}
