import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesContractComponent } from './services-contract.component';

describe('ServicesContractComponent', () => {
  let component: ServicesContractComponent;
  let fixture: ComponentFixture<ServicesContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
