export interface ExpenditureContractModel {
    expenditureContractSelect: any;
    stationList: any;
    NameOfLicesee: string;
    ownerName: string;
    phoneNumber: string;
    emailId: string ;
    NoofYearsContract: number;
    modeofPaymentType: string;
    conStartdate: Date ;
    conEnddate: Date ;
    EstimatedValue: number ;
    AcceptedRate: number ;
    securityDepositAmount: number;
    securityDepositTxnID: string ;
    securityTxnDate: Date;
    emdDepositAmount: number;
    emdRemarks: string;
}
