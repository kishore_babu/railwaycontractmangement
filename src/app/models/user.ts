export interface Loggedinuser {
    userId: string;
    username: string;
    password: string;
    name: string;
    roleid: number;
    token?: string;
}
