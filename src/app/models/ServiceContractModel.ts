export interface ServiceContractModel {
    serviceContractSelect: any;
    stationList: any;
    NameOfLicesee: string;
    ownerName: string;
    phoneNumber: string;
    emailId: string ;
    NoofYearsContract: number;
    modeofPaymentType: string;
    conStartdate: Date ;
    conEnddate: Date ;
    PercentageCommission: number ;
  }