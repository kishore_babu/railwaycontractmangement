export interface EarningsContractModel {
  [x: string]: any;
    earningContractSelect: any;
    stationList: any;
    NameOfLicesee: string;
    ownerName: string;
    phoneNumber: string;
    emailId: string ;
    NoofYearsContract: number;
    modeofPaymentType: string;
    conStartdate: Date ;
    conEnddate: Date ;
    contractfee: number ;
    licenscefree: number ;
    securityDepositAmount: number;
    securityDepositTxnID: string ;
    securityTxnDate: Date;
    emdDepositAmount: number;
    emdRemarks: string;
}

export class Earnings implements EarningsContractModel{
 
}