import { TestBed } from '@angular/core/testing';

import { SationsMasterService } from './sations-master.service';

describe('SationsMasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SationsMasterService = TestBed.get(SationsMasterService);
    expect(service).toBeTruthy();
  });
});
