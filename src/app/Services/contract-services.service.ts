import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {EarningsContractModel } from '../models/EarningsContractModel';
import {ExpenditureContractModel } from '../models/ExpenditureContractModel';

import { environment } from 'src/environments/environment';
import { ServiceContractModel } from '../models/ServiceContractModel';
@Injectable({
  providedIn: 'root'
})
export class ContractServicesService {

  constructor(private http: HttpClient) { }

  createEarningsContract(earningscontract: EarningsContractModel) {
    return this.http.post<any>(environment.apiHost + environment.api_ContractMethod + 'earningsContract'
    , earningscontract);
  }
  getEarningsContract() {
    return this.http.get<EarningsContractModel>(environment.apiHost + environment.api_ContractMethod + 'getEarningContract');
  }
  createExpenditureContract(expenditurescontract: ExpenditureContractModel) {
    return this.http.post<any>(environment.apiHost + environment.api_ContractMethod + 'expenditureContract'
    , expenditurescontract);
  }
  getExpenditureContract() {
    return this.http.get<ExpenditureContractModel>(environment.apiHost + environment.api_ContractMethod + 'getExpenditureContract');
  }
  createServiceContract(serviceContract: ServiceContractModel) {
    return this.http.post<any>(environment.apiHost + environment.api_ContractMethod + 'serviceContract'
    , serviceContract);
  }

  getServiceContract() {
    return this.http.get<ServiceContractModel>(environment.apiHost + environment.api_ContractMethod + 'getServiceContract');
  }
}
