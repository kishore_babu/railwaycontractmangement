import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Stations } from '../models/Stations';
@Injectable({
  providedIn: 'root'
})
export class SationsMasterService {
  constructor() {  }
  public getStationList(): any{
    let stationData: Stations[] = [
      {id : 1, StationName : 'Tirupathi', StationCode : 'TPTY'},
      {id : 2, StationName : 'Guntakal Junction', StationCode : 'GTL'}
    ];
    
    return stationData;
  }
}
