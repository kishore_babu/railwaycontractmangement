import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {ReactiveFormsModule} from '@angular/forms';
import {FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import {HomepageComponent} from './homepage/homepage.component';
import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { AuthService } from './auth/auth.service';
import { AuthGuard } from './auth/auth.guard';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { EarningsContractsComponent } from './earnings-contracts/earnings-contracts.component';
import { ExpenditureContractComponent } from './expenditure-contract/expenditure-contract.component';
import { ServicesContractComponent } from './services-contract/services-contract.component';
import { ExceluploadComponent } from './excelupload/excelupload.component';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    LoginComponent,
    MenuComponent,
    EarningsContractsComponent,
    ExpenditureContractComponent,
    ServicesContractComponent,
    ExceluploadComponent,
    DashboardComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
