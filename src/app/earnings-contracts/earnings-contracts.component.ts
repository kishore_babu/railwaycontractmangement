 import { Component, OnInit } from '@angular/core';
 import {FormGroup, FormBuilder, Validators } from '@angular/forms';
 import {SationsMasterService} from '../Services/sations-master.service';
 import { ContractServicesService } from '../Services/contract-services.service';
 @Component({
  selector: 'app-earnings-contracts',
  templateUrl: './earnings-contracts.component.html',
  styleUrls: ['./earnings-contracts.component.css']
})
export class EarningsContractsComponent implements OnInit {
  constructor(private fb: FormBuilder, private contractService: ContractServicesService,
              private stationDataService: SationsMasterService) { }
  earningsContractForm: FormGroup;
  contractStartDate: any;
  contractEndDate: any;
  securityTranscationDate: any;
  message: any = '';
  earningContractTypes: any;
  stationMasterData: any;

btn_Text='Add Contract';

  displayListData = false;
  earningContractList ;
  ngOnInit(): void {
    this.getEarningContractList();
    this.getStationMaster();
    this.earningContractTypes = [
        { id: 1 , Name: 'Parking'},
        { id: 2 , Name: 'Commercial publicity'},
        { id: 3 , Name: 'ATM'},
        { id: 4 , Name: 'Cloak Room'},
        { id: 5 , Name: 'Catering'},
        { id: 6 , Name: 'Pay & Use Toilets'},
        { id: 7 , Name: 'Leasing of SLR & VP'},
        { id: 8 , Name: 'AC waiting halls'},
        { id: 9 , Name: 'Premium Lounge'},
        { id: 10 , Name: 'Multi Purpose Stalls'},
        { id: 11 , Name: 'Commercial Plots'},
        { id: 12 , Name: 'Other Misc. (NINFRIS)'}
      ];

    this.earningsContractForm = this.fb.group(
    {
      NameOfLicesee : ['', [Validators.required, Validators.minLength(2)]],
      ownerName : ['', [Validators.required, Validators.minLength(2)]],
      phoneNumber : ['', [Validators.required, Validators.minLength(2)]],
      emailId : ['', [Validators.required, Validators.minLength(2), Validators.email]],
      NoofYearsContract : [null, [Validators.required, Validators.minLength(2)]],
      modeofPaymentType : [null, [Validators.required]],
      conStartdate : [null, [Validators.required]],
      conEnddate : [null, [Validators.required]],
      contractfee : [0, [Validators.required, Validators.minLength(2)]],
      licenscefree : [0, [Validators.required, Validators.minLength(2)]],
      earningContractSelect : [ null, [Validators.required]],
      StationList : [null, [ Validators.required]],
      securityDepositAmount: [0, [Validators.required, Validators.minLength(2)]],
      securityDepositTxnID: ['', [Validators.required, Validators.minLength(2)]],
      securityTxnDate: [null, [ Validators.required]],
      emdDepositAmount: [0, [Validators.required, Validators.minLength(2)]],
      emdRemarks: ['', [Validators.required, Validators.minLength(2)]]

    });
  }
  getStationMaster() {
    this.stationMasterData =
    [ {id : 1, StationName : 'Tirupathi', StationCode : 'TPTY'},
    {id : 2, StationName : 'Guntakal Junction', StationCode : 'GTL'}];
    // this.stationDataService.getStationList().subscribe(
    //   result => {
    //     console.log(result);
    //     this.stationMasterData = result;
    //     console.log(this.stationMasterData);
    //   }, error => {
    //    // this.message = 'Error in creating shop';
    //   }
    // );
  }

  maketwodigits(value: any) {
    let finalvalue;
    if ( value < 10) {
      finalvalue = value = '0' + value;
    } else {
      finalvalue = value;
    }
    return finalvalue;
  }
   createEarningsContract() {
    console.log('testing ');
    console.log(this.earningsContractForm.value);
    if (this.earningsContractForm.valid) {
      console.log(this.earningsContractForm.value);
      this.earningsContractForm.patchValue(
        {
          conStartdate : ( this.contractStartDate.year + '-'
                        + this.maketwodigits(this.contractStartDate.month) + '-'
                        + this.maketwodigits(this.contractStartDate.day)),
          conEnddate : (this.contractEndDate.year + '-'
                  + this.maketwodigits(this.contractEndDate.month) + '-'
                  + this.maketwodigits(this.contractEndDate.day)),
          securityTxnDate : (this.securityTranscationDate.year + '-'
                 + this.maketwodigits(this.securityTranscationDate.month) + '-'
                 + this.maketwodigits(this.securityTranscationDate.day))
        }
        );
      this.contractService.createEarningsContract(this.earningsContractForm.value).subscribe(
        result => {
          console.log(result);
          this.message = 'Shop created';
          this.earningsContractForm.reset();
        },
        (error: any) => {
          this.message = 'Error in creating EarningsContract';
        });
    }
  }
  getEarningContractList() {
    if(this.displayListData === false) {
      this.btn_Text = 'Add New Contract';
      this.contractService.getEarningsContract().subscribe(data => {
        console.log(data);
        this.earningContractList = data;
      },
      (error: any) => {
        this.message = 'Error in getting data';
      });
    } else {
      this.btn_Text = 'View Contracts';
    }
    this.displayListData = !this.displayListData;
  }
}
