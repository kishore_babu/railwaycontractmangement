import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarningsContractsComponent } from './earnings-contracts.component';

describe('EarningsContractsComponent', () => {
  let component: EarningsContractsComponent;
  let fixture: ComponentFixture<EarningsContractsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarningsContractsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarningsContractsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
