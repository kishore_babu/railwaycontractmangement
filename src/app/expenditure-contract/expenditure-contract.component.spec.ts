import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenditureContractComponent } from './expenditure-contract.component';

describe('ExpenditureContractComponent', () => {
  let component: ExpenditureContractComponent;
  let fixture: ComponentFixture<ExpenditureContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpenditureContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpenditureContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
