import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators } from '@angular/forms';

import {SationsMasterService} from '../Services/sations-master.service';
import { ContractServicesService } from '../Services/contract-services.service';

@Component({
  selector: 'app-expenditure-contract',
  templateUrl: './expenditure-contract.component.html',
  styleUrls: ['./expenditure-contract.component.css']
})

export class ExpenditureContractComponent implements OnInit {

  expenditureContractForm: FormGroup;
  constructor(private fb: FormBuilder, private contractService: ContractServicesService) { }
  contractStartDate: any;
  contractEndDate: any;
  securityTranscationDate: any;
  message: any = '';
  stationMasterData: any;
  expenditureContractTypes: any;
  displayListData = false;
  expenditureContractList: any;
  btn_Text = 'Add New Contract';
  ngOnInit(): void {

    this.getExpenditureContractList();
    this.getStationMaster();
    this.expenditureContractTypes =
    [
      { id: 1 , Name: 'Parcels Handling'},
      { id: 2 , Name: 'Face to Face Enquiry'},
      { id: 3 , Name: 'Dhobi (Washing of Linen)'},
      { id: 4 , Name: 'Vehile Hiring'},
      { id: 5 , Name: 'TTEs Rest House maintenance'},
      { id: 6 , Name: 'TTH ORH maintenance'},
    ];

    this.expenditureContractForm = this.fb.group(
      {
        NameOfLicesee : ['', [Validators.required, Validators.minLength(2)]],
        ownerName : ['', [Validators.required, Validators.minLength(2)]],
        phoneNumber : ['', [Validators.required, Validators.minLength(2)]],
        emailId : ['', [Validators.required, Validators.minLength(2), Validators.email]],
        NoofYearsContract : ['', [Validators.required, Validators.minLength(2)]],
        modeofPaymentType : [null, [Validators.required]],
        conStartdate : [null, [Validators.required]],
        conEnddate : [null, [Validators.required]],
        EstimatedValue : [0, [Validators.required, Validators.minLength(2)]],
        AcceptedRate : [0, [Validators.required, Validators.minLength(2)]],
        expenditureContractSelect : [ null, [Validators.required]],
        StationList : [null, [ Validators.required]],
        securityDepositAmount: [0, [Validators.required, Validators.minLength(2)]],
        securityDepositTxnID: ['', [Validators.required, Validators.minLength(2)]],
        securityTxnDate: [null, [Validators.required]],
        emdDepositAmount: [0, [Validators.required, Validators.minLength(2)]],
        emdRemarks: ['', [Validators.required, Validators.minLength(2)]]
      });
  }

  getStationMaster() {
    this.stationMasterData =
    [ {id : 1, StationName : 'Tirupathi', StationCode : 'TPTY'},
    {id : 2, StationName : 'Guntakal Junction', StationCode : 'GTL'}];
  }
  maketwodigits(value: any) {
    let finalvalue;
    if ( value < 10) {
      finalvalue = value = '0' + value;
    } else {
      finalvalue = value;
    }
    return finalvalue;
  }
createExpenditureContract() {
    console.log('testing ');
    console.log(this.expenditureContractForm.value);
    if (this.expenditureContractForm.valid) {
      console.log(this.expenditureContractForm.value);
      this.expenditureContractForm.patchValue(
        {
          conStartdate : ( this.contractStartDate.year + '-'
                        + this.maketwodigits(this.contractStartDate.month) + '-'
                        + this.maketwodigits(this.contractStartDate.day)),
          conEnddate : (this.contractEndDate.year + '-'
                  + this.maketwodigits(this.contractEndDate.month) + '-'
                  + this.maketwodigits(this.contractEndDate.day)),
          securityTxnDate : (this.securityTranscationDate.year + '-'
                 + this.maketwodigits(this.securityTranscationDate.month) + '-'
                 + this.maketwodigits(this.securityTranscationDate.day))
        }
        );
      this.contractService.createExpenditureContract(this.expenditureContractForm.value).subscribe(
        result => {
          console.log(result);
          this.message = 'Expenditure Contract created';
          this.expenditureContractForm.reset();
        },
        (error: any) => {
          this.message = 'Error in creating shop';
        });
    }
  }
  getExpenditureContractList() {
    if ( this.displayListData === false){
      this.btn_Text = 'Add New Contract';
      this.contractService.getExpenditureContract().subscribe(data => {
        console.log(data);
        this.expenditureContractList = data;
      },
      (error: any) => {
        this.message = 'Error in getting data';
      });
    } else {
      this.btn_Text = 'View Contracts';
    }
    this.displayListData = !this.displayListData;
  }
}
